/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class EmployerDAO {
	public static Employer loadEmployerByORMID(String matricola) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return loadEmployerByORMID(session, matricola);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Employer getEmployerByORMID(String matricola) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return getEmployerByORMID(session, matricola);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Employer loadEmployerByORMID(String matricola, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return loadEmployerByORMID(session, matricola, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Employer getEmployerByORMID(String matricola, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return getEmployerByORMID(session, matricola, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Employer loadEmployerByORMID(PersistentSession session, String matricola) throws PersistentException {
		try {
			return (Employer) session.load(vista_architetturale_gestoresmartfarm.entity2.Employer.class, matricola);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Employer getEmployerByORMID(PersistentSession session, String matricola) throws PersistentException {
		try {
			return (Employer) session.get(vista_architetturale_gestoresmartfarm.entity2.Employer.class, matricola);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Employer loadEmployerByORMID(PersistentSession session, String matricola, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Employer) session.load(vista_architetturale_gestoresmartfarm.entity2.Employer.class, matricola, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Employer getEmployerByORMID(PersistentSession session, String matricola, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Employer) session.get(vista_architetturale_gestoresmartfarm.entity2.Employer.class, matricola, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEmployer(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return queryEmployer(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEmployer(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return queryEmployer(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Employer[] listEmployerByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return listEmployerByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Employer[] listEmployerByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return listEmployerByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEmployer(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.Employer as Employer");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEmployer(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.Employer as Employer");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Employer", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Employer[] listEmployerByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryEmployer(session, condition, orderBy);
			return (Employer[]) list.toArray(new Employer[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Employer[] listEmployerByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryEmployer(session, condition, orderBy, lockMode);
			return (Employer[]) list.toArray(new Employer[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Employer loadEmployerByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return loadEmployerByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Employer loadEmployerByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return loadEmployerByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Employer loadEmployerByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Employer[] employers = listEmployerByQuery(session, condition, orderBy);
		if (employers != null && employers.length > 0)
			return employers[0];
		else
			return null;
	}
	
	public static Employer loadEmployerByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Employer[] employers = listEmployerByQuery(session, condition, orderBy, lockMode);
		if (employers != null && employers.length > 0)
			return employers[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateEmployerByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return iterateEmployerByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateEmployerByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return iterateEmployerByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateEmployerByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.Employer as Employer");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateEmployerByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.Employer as Employer");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Employer", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Employer createEmployer() {
		return new vista_architetturale_gestoresmartfarm.entity2.Employer();
	}
	
	public static boolean save(vista_architetturale_gestoresmartfarm.entity2.Employer employer) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().saveObject(employer);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(vista_architetturale_gestoresmartfarm.entity2.Employer employer) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().deleteObject(employer);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(vista_architetturale_gestoresmartfarm.entity2.Employer employer) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession().refresh(employer);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(vista_architetturale_gestoresmartfarm.entity2.Employer employer) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession().evict(employer);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
