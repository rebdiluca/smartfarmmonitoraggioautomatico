/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

import java.util.ArrayList;
import java.text.DateFormat;
import java.util.*;

import org.orm.PersistentException;

public class SmartFarm {
	public SmartFarm() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_SMARTFARM_COLTIVAZIONI) {
			return ORM_coltivazioni;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int ID;
	
	private java.util.Set ORM_coltivazioni = new java.util.HashSet();
	
	private volatile static SmartFarm smartFarm = null;
	
	private static String lastDay;
	
	public static SmartFarm caricaSmartFarm() throws PersistentException {
		
		//TODO: Implement Method
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		Date today = calendar.getTime();
		int format = DateFormat.SHORT;
		DateFormat dateFormat = DateFormat.getDateInstance(format, Locale.ITALY);
		if (lastDay == null){
			System.out.println("Last day non presente --- PRIMO ACCESSO");
			lastDay = dateFormat.format(today);
			synchronized(SmartFarm.class){
				if(smartFarm == null){
					//smartFarm = SmartFarmDAO.getSmartFarmByORMID(1);
					smartFarm = new SmartFarm();
					smartFarm.setORM_Coltivazioni(SmartFarmDAO.getSmartFarmByORMID(1).getORM_Coltivazioni());
				}
			}
		} else if(lastDay.compareTo(dateFormat.format(today)) != 0){
			System.out.println("Last day presente --- N-ESIMO ACCESSO");
			synchronized(SmartFarm.class){
				if(smartFarm == null){
					//smartFarm = SmartFarmDAO.getSmartFarmByORMID(1);
					smartFarm = new SmartFarm();
					smartFarm.setORM_Coltivazioni(SmartFarmDAO.getSmartFarmByORMID(1).getORM_Coltivazioni());
				}
			}
		}
		
		return smartFarm;
	}
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	private void setORM_Coltivazioni(java.util.Set value) {
		this.ORM_coltivazioni = value;
	}
	
	private java.util.Set getORM_Coltivazioni() {
		return ORM_coltivazioni;
	}
	
	public final vista_architetturale_gestoresmartfarm.entity2.ColtivazioneSetCollection coltivazioni = new vista_architetturale_gestoresmartfarm.entity2.ColtivazioneSetCollection(this, _ormAdapter, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_SMARTFARM_COLTIVAZIONI, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public Coltivazione getColtivazioniAttive(int idSerra) {
		Coltivazione coltivazione = null;
		boolean trovato = false;
		
		Iterator<Coltivazione> it = this.ORM_coltivazioni.iterator();
		
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		Date today = calendar.getTime();
		
		while(it.hasNext() && !trovato){
			
			Coltivazione temp = it.next();
			
			if(temp.getDataInizio().before(today) && temp.getDataFine().after(today) && temp.getSerra().getID() == idSerra) {
				coltivazione = temp;
				trovato = true;
			}
			
		}
		
		return coltivazione;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
