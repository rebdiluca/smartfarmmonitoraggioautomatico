/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

public class Lettura {
	public Lettura() {
	}
	
	private int ID;
	
	private java.util.Date data;
	
	private float temperatura;
	
	private float umiditaAria;
	
	private float umiditaSuolo;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setData(java.util.Date value) {
		this.data = value;
	}
	
	public java.util.Date getData() {
		return data;
	}
	
	public void setTemperatura(float value) {
		this.temperatura = value;
	}
	
	public float getTemperatura() {
		return temperatura;
	}
	
	public void setUmiditaAria(float value) {
		this.umiditaAria = value;
	}
	
	public float getUmiditaAria() {
		return umiditaAria;
	}
	
	public void setUmiditaSuolo(float value) {
		this.umiditaSuolo = value;
	}
	
	public float getUmiditaSuolo() {
		return umiditaSuolo;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
