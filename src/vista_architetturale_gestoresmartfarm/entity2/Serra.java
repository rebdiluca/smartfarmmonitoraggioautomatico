/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;
import java.util.stream.Stream;

import org.hibernate.Session;
import org.json.JSONException;
import org.json.JSONObject;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import org.json.JSONObject;

public class Serra {
	public Serra() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_SERRA_EMPLOYERS) {
			return ORM_employers;
		}
		else if (key == vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_SERRA_SENSORI) {
			return ORM_sensori;
		}
		else if (key == vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_SERRA_LETTURE) {
			return ORM_letture;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int ID;
	
	private java.util.Set ORM_employers = new java.util.HashSet();
	
	private java.util.Set ORM_sensori = new java.util.HashSet();
	
	private java.util.Set ORM_letture = new java.util.HashSet();
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	private void setORM_Employers(java.util.Set value) {
		this.ORM_employers = value;
	}
	
	private java.util.Set getORM_Employers() {
		return ORM_employers;
	}
	
	public final vista_architetturale_gestoresmartfarm.entity2.EmployerSetCollection employers = new vista_architetturale_gestoresmartfarm.entity2.EmployerSetCollection(this, _ormAdapter, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_SERRA_EMPLOYERS, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Sensori(java.util.Set value) {
		this.ORM_sensori = value;
	}
	
	private java.util.Set getORM_Sensori() {
		return ORM_sensori;
	}
	
	public final vista_architetturale_gestoresmartfarm.entity2.SensoreSetCollection sensori = new vista_architetturale_gestoresmartfarm.entity2.SensoreSetCollection(this, _ormAdapter, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_SERRA_SENSORI, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Letture(java.util.Set value) {
		this.ORM_letture = value;
	}
	
	private java.util.Set getORM_Letture() {
		return ORM_letture;
	}
	
	public final vista_architetturale_gestoresmartfarm.entity2.LetturaSetCollection letture = new vista_architetturale_gestoresmartfarm.entity2.LetturaSetCollection(this, _ormAdapter, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_SERRA_LETTURE, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public void salvaLettura(JSONObject lettura) throws PersistentException {
		
		PersistentTransaction transaction = SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession().beginTransaction();
		/*
		try {
			Lettura lett = LetturaDAO.createLettura();
			
			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
			Date today = calendar.getTime();
			lett.setData(today);
			lett.setTemperatura(Float.parseFloat((String)lettura.get("temperature")));
			lett.setUmiditaAria(Float.parseFloat((String)lettura.get("humidity")));
			lett.setUmiditaSuolo(Float.parseFloat((String)lettura.get("soilMoisture")));
			
		
			this.letture.add(lett);
			
			SerraDAO.save(this);
		
			transaction.commit();
			
		}catch(Exception e) {
			e.printStackTrace();
			//transaction.rollback();
		}
		*/
		
		Lettura lett = new Lettura();
		
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		Date today = calendar.getTime();
		lett.setData(today);
		try {
			lett.setTemperatura(Float.parseFloat((String)lettura.get("temperature")));
			lett.setUmiditaAria(Float.parseFloat((String)lettura.get("humidity")));
			lett.setUmiditaSuolo(Float.parseFloat((String)lettura.get("soilMoisture")));
			
			this.ORM_letture.add(lett);
			
			this.letture.add(lett);
			
			SerraDAO.save(this);
			
			transaction.commit();
			
			SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession().close();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void addIntrusione(java.util.Date data, String tipo, ArrayList<Integer> img, java.util.Date ora) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public ArrayList<String> getNotifyInformationEmployers() {
		
		ArrayList<String> recapiti =  new ArrayList<String>();
		
		Iterator<Employer> it = this.employers.getIterator();
		
		while(it.hasNext()) {
			Employer e = it.next();
			
			recapiti.add(e.getRecapito());
			
		}
		
		return recapiti;
		
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
