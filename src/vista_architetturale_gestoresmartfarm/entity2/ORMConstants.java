/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

public interface ORMConstants extends org.orm.util.ORMBaseConstants {
	final int KEY_COLTIVAZIONE_AMMINISTRATORE = -1624897687;
	
	final int KEY_COLTIVAZIONE_SERRA = 1719107559;
	
	final int KEY_SERRA_EMPLOYERS = -1858392248;
	
	final int KEY_SERRA_LETTURE = 850306879;
	
	final int KEY_SERRA_SENSORI = -1532678625;
	
	final int KEY_SMARTFARM_COLTIVAZIONI = 268926523;
	
}
