/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

public class Coltivazione {
	public Coltivazione() {
	}
	
	private int ID;
	
	private vista_architetturale_gestoresmartfarm.entity2.Serra serra;
	
	private vista_architetturale_gestoresmartfarm.entity2.Amministratore amministratore;
	
	private java.util.Date dataInizio;
	
	private java.util.Date dataFine;
	
	private String tipologia;
	
	private float tempMin;
	
	private float tempMax;
	
	private float humMin;
	
	private float humMax;
	
	private float soilMin;
	
	private float soilMax;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setDataInizio(java.util.Date value) {
		this.dataInizio = value;
	}
	
	public java.util.Date getDataInizio() {
		return dataInizio;
	}
	
	public void setDataFine(java.util.Date value) {
		this.dataFine = value;
	}
	
	public java.util.Date getDataFine() {
		return dataFine;
	}
	
	public void setTipologia(String value) {
		this.tipologia = value;
	}
	
	public String getTipologia() {
		return tipologia;
	}
	
	public void setTempMin(float value) {
		this.tempMin = value;
	}
	
	public float getTempMin() {
		return tempMin;
	}
	
	public void setTempMax(float value) {
		this.tempMax = value;
	}
	
	public float getTempMax() {
		return tempMax;
	}
	
	public void setHumMin(float value) {
		this.humMin = value;
	}
	
	public float getHumMin() {
		return humMin;
	}
	
	public void setHumMax(float value) {
		this.humMax = value;
	}
	
	public float getHumMax() {
		return humMax;
	}
	
	public void setSoilMin(float value) {
		this.soilMin = value;
	}
	
	public float getSoilMin() {
		return soilMin;
	}
	
	public void setSoilMax(float value) {
		this.soilMax = value;
	}
	
	public float getSoilMax() {
		return soilMax;
	}
	
	public void setAmministratore(vista_architetturale_gestoresmartfarm.entity2.Amministratore value) {
		this.amministratore = value;
	}
	
	public vista_architetturale_gestoresmartfarm.entity2.Amministratore getAmministratore() {
		return amministratore;
	}
	
	public void setSerra(vista_architetturale_gestoresmartfarm.entity2.Serra value) {
		this.serra = value;
	}
	
	public vista_architetturale_gestoresmartfarm.entity2.Serra getSerra() {
		return serra;
	}
	
	public String getNotifyInformationAdmin() {
		return this.getAmministratore().getRecapito();
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
