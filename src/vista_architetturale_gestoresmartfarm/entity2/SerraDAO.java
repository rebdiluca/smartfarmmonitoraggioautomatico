/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class SerraDAO {
	public static Serra loadSerraByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return loadSerraByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Serra getSerraByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return getSerraByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Serra loadSerraByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return loadSerraByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Serra getSerraByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return getSerraByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Serra loadSerraByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Serra) session.load(vista_architetturale_gestoresmartfarm.entity2.Serra.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Serra getSerraByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Serra) session.get(vista_architetturale_gestoresmartfarm.entity2.Serra.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Serra loadSerraByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Serra) session.load(vista_architetturale_gestoresmartfarm.entity2.Serra.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Serra getSerraByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Serra) session.get(vista_architetturale_gestoresmartfarm.entity2.Serra.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List querySerra(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return querySerra(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List querySerra(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return querySerra(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Serra[] listSerraByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return listSerraByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Serra[] listSerraByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return listSerraByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List querySerra(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.Serra as Serra");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List querySerra(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.Serra as Serra");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Serra", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Serra[] listSerraByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = querySerra(session, condition, orderBy);
			return (Serra[]) list.toArray(new Serra[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Serra[] listSerraByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = querySerra(session, condition, orderBy, lockMode);
			return (Serra[]) list.toArray(new Serra[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Serra loadSerraByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return loadSerraByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Serra loadSerraByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return loadSerraByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Serra loadSerraByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Serra[] serras = listSerraByQuery(session, condition, orderBy);
		if (serras != null && serras.length > 0)
			return serras[0];
		else
			return null;
	}
	
	public static Serra loadSerraByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Serra[] serras = listSerraByQuery(session, condition, orderBy, lockMode);
		if (serras != null && serras.length > 0)
			return serras[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateSerraByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return iterateSerraByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateSerraByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession();
			return iterateSerraByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateSerraByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.Serra as Serra");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateSerraByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From vista_architetturale_gestoresmartfarm.entity2.Serra as Serra");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Serra", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Serra createSerra() {
		return new vista_architetturale_gestoresmartfarm.entity2.Serra();
	}
	
	public static boolean save(vista_architetturale_gestoresmartfarm.entity2.Serra serra) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().saveObject(serra);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(vista_architetturale_gestoresmartfarm.entity2.Serra serra) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().deleteObject(serra);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(vista_architetturale_gestoresmartfarm.entity2.Serra serra) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession().refresh(serra);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(vista_architetturale_gestoresmartfarm.entity2.Serra serra) throws PersistentException {
		try {
			vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager.instance().getSession().evict(serra);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
