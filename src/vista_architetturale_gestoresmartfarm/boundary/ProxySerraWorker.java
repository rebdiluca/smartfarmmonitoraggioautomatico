package vista_architetturale_gestoresmartfarm.boundary;

import javax.security.auth.login.Configuration;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.PersistentTransaction;
import org.hibernate.*;

import com.mysql.cj.xdevapi.SessionFactory;

import vista_architetturale_gestoresmartfarm.coordinator.CoordinatorMonitoraggio;
import vista_architetturale_gestoresmartfarm.entity2.SmartFarmMonitoraggioAutomaticoPersistentManager;


public class ProxySerraWorker extends Thread{
	
	private MqttMessage message; 
	private String topicRicezione;
	private String topicInvioComandi;
	private String broker;
	private String clientId;

	
	public ProxySerraWorker(String topic, MqttMessage msg, String topicInvioComandi, String broker, String clientId) {
		this.message = msg;
		this.topicRicezione = topic;
		this.topicInvioComandi = topicInvioComandi;
		this.broker = broker;
		this.clientId = clientId;
	}
	
	public void run() {
		
		
		
		try {
			
			JSONObject json = (JSONObject) new JSONTokener(message.toString()).nextValue();
			String requestType = topicRicezione.substring(topicRicezione.lastIndexOf("/") + 1);
			int idSerra = json.getInt("idSerra");
			CoordinatorMonitoraggio coord = CoordinatorMonitoraggio.getInstance();

			switch(requestType){
			
				case "letture":
					
					
					System.out.println("	Messaggio tipo 'lettura' .");
					int esito = coord.gestioneLettura(idSerra, json);
					
					if(esito == 3 || esito == 5 || esito == 6 || esito == 7){
						
				        String content = new String("AttivaIrrigazione");
				        
				        MemoryPersistence persistence = new MemoryPersistence();

				        try {
				            MqttClient sampleClient = new MqttClient(broker, clientId+"_Comandi", persistence);
				            MqttConnectOptions connOpts = new MqttConnectOptions();
				            connOpts.setCleanSession(true);
				          
				            sampleClient.connect(connOpts);
				            MqttMessage message = new MqttMessage();

							message.setPayload(content.getBytes());

				            sampleClient.publish(this.topicInvioComandi+"/serra"+idSerra, message);
				            
				            System.out.println("	["+this.topicInvioComandi+"] Comando pubblicato.\n\n");
				            
				            sampleClient.disconnect();
				            
				        } catch(MqttException me) {
				            System.out.println("reason "+me.getReasonCode());
				            System.out.println("msg "+me.getMessage());
				            System.out.println("loc "+me.getLocalizedMessage());
				            System.out.println("cause "+me.getCause());
				            System.out.println("excep "+me);
				            me.printStackTrace();
				        }
						
					}
					
					break;
	
				case "statusAttuatori":
					
					System.out.println("	Messaggio tipo 'status attuatore' .");
	
					coord.gestioneStatusAttuatori(idSerra, json);
					
					break;
			
				default:
					System.out.println("No match type");
					
			}
		

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}
	
}
