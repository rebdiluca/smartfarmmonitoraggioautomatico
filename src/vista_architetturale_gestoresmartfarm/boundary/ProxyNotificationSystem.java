package vista_architetturale_gestoresmartfarm.boundary;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class ProxyNotificationSystem {

	private volatile static ProxyNotificationSystem proxy = null;
	private final static String USER_AGENT = "";

	private ProxyNotificationSystem() {
	}
	
	public static ProxyNotificationSystem getInstance() {
		if (proxy == null) {
			synchronized(ProxySerra.class){ 
				if(proxy == null){
					proxy = new ProxyNotificationSystem();
				}
			}
		}
		return proxy;
	}
	
	public void invioSegnalazioneRange(String admininfo, ArrayList<String> employersinfo, String notifica) {
		
		System.out.println(notifica);
		employersinfo.add(admininfo);
		String url;
		URL obj;
		
		try {
			Iterator<String> it = employersinfo.iterator();
			while(it.hasNext()){
				url = "https://api.rpinotify.it/message/" + it.next() +"/";
				obj = new URL(url);
				HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
	
				//add request header
				con.setRequestMethod("POST");
				con.setRequestProperty("User-Agent", "MOZILLA/5.0");
				con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	
				String urlParameters = "text=" + notifica;
				
				// Send post request
				con.setDoOutput(true);
				DataOutputStream wr;
				wr = new DataOutputStream(con.getOutputStream());
				
				wr.writeBytes(urlParameters);
				wr.flush();
				wr.close();
				
				int responseCode = con.getResponseCode();
				System.out.println("\nSending 'POST' request to URL : " + url);
				System.out.println("Post parameters : " + urlParameters);
				System.out.println("Response Code : " + responseCode);
	
				BufferedReader in = new BufferedReader(
				        new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
	
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				
				//print result
				System.out.println(response.toString());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void invioSegnalazioneAttuatore(String admininfo, ArrayList<String> employersinfo, String status) {
	
		System.out.println("	"+status);
		
		String command = "python provaBot.py ";
		Process p = null;
		try {
			p = Runtime.getRuntime().exec(command + admininfo + " " + status);
			
			Iterator<String> it = employersinfo.iterator();
			
			while(it.hasNext())
				p = Runtime.getRuntime().exec(command + it.next() + " " + status);
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
	}
	
}
