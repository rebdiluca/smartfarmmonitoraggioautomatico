package vista_architetturale_gestoresmartfarm.boundary;

import java.util.ArrayList;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;


public class ProxySerra implements MqttCallback{
	
    private String broker;
    private String clientId;
    private String topicInvioComandi;
    
	private volatile static ProxySerra proxy = null;

	private ProxySerra() {
		
	}
	
	public static ProxySerra getInstance() {
		if (proxy == null) {
			synchronized(ProxySerra.class){ 
				if(proxy == null){
					proxy = new ProxySerra();
				}
			}
		}
		return proxy;
	}

	
	public String getTopicInvioComandi() {
		return topicInvioComandi;
	}

	public void setTopicInvioComandi(String topicInvioComandi) {
		this.topicInvioComandi = topicInvioComandi;
	}

	public String getBroker() {
		return broker;
	}

	public void setBroker(String broker) {
		this.broker = broker;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}


	@Override
	public void connectionLost(Throwable arg0) {
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
	}

	@Override
	public void messageArrived(String topicRicezione, MqttMessage msg) throws Exception {
		// TODO Auto-generated method stub

		System.out.println("["+topicRicezione+"] Messaggio Ricevuto.");
		
		ProxySerraWorker worker = new ProxySerraWorker(topicRicezione, msg, this.topicInvioComandi, this.broker, this.clientId);
		worker.start();
		
	}
	
	
	public void subscribe(String topic) {
		
		MemoryPersistence persistence = new MemoryPersistence();

		try{
			
			MqttClient sampleClient = new MqttClient(this.broker, this.clientId, persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true);

			sampleClient.connect(connOpts);
			System.out.println("Connesso al broker: " + this.broker);

			sampleClient.setCallback(this);
			sampleClient.subscribe(topic);

			System.out.println("[Sottoscrizione]:" + topic + " topic letture/status.\n\n");

		} catch (MqttException me) {
			
			System.out.println(me);
			
		}
		
	}
	
}
