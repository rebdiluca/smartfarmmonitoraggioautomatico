
package vista_architetturale_gestoresmartfarm.coordinator;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.orm.PersistentException;

import vista_architetturale_gestoresmartfarm.boundary.ProxyNotificationSystem;
import vista_architetturale_gestoresmartfarm.business_logic.GestioneMonitoraggioBL;
import vista_architetturale_gestoresmartfarm.entity2.*;

public class CoordinatorMonitoraggio {
	
	private volatile static CoordinatorMonitoraggio c = null;
	
	private CoordinatorMonitoraggio(){
	}

	public static CoordinatorMonitoraggio getInstance(){
		
		if (c == null) {
			synchronized(CoordinatorMonitoraggio.class){
				if(c == null){
					c = new CoordinatorMonitoraggio();
				}
			}
		}
		
		return c;
	}

	public int gestioneLettura(int idSerra, JSONObject lettura){
		
		int esito = 0;
		
		GestioneMonitoraggioBL gestoreBL = new GestioneMonitoraggioBL();
		
		Coltivazione coltivazione = gestoreBL.getColtivazione(idSerra);
		
		if ( coltivazione != null ) {
			
			Serra serra = coltivazione.getSerra();
			
			System.out.println("	Serra prelevata: " + serra.getID());
			
			gestoreBL.creaLettura(lettura, serra);
			
			System.out.println("	[Lettura Serra "+idSerra+"] Lettura salvata.");
			
			esito = gestoreBL.checkRange(lettura, coltivazione);
			
			if ( esito == 0 ) {
				
				System.out.println("	[Lettura Serra "+idSerra+"] Lettura OK.");
				
			}else if ( esito > 0) {
				
				String admininfo = coltivazione.getNotifyInformationAdmin();
				ArrayList<String> employersinfo = serra.getNotifyInformationEmployers();
				
				ProxyNotificationSystem proxy = ProxyNotificationSystem.getInstance();
				
				String notifica = new String("");
				
				try {
					
					switch(esito){
						case 1:
							notifica = "Serra " + serra.getID() + ":	[Esito] Temperatura out of range: " + lettura.getDouble("temperature") + "." ;
							break;
						case 2:
							notifica = "Serra " + serra.getID() + ":	[Esito] Umidita out of range: " + lettura.getDouble("humidity") + "." ;
							break;
						case 3:
							notifica = "Serra " + serra.getID() + ":	[Esito] Umidita suolo out of range: " + lettura.getDouble("soilMoisture") + "." ;
							break;
						case 4:
							notifica = "Serra " + serra.getID() + ":	[Esito] Temperatura out of range: " + lettura.getDouble("temperature") + "" + "	Umidita out of range: " + lettura.getDouble("humidity")+".";
							break;
						case 5: 
							notifica = "Serra " + serra.getID() + ":	[Esito] Temperatura out of range: " + lettura.getDouble("temperature") + "" + "	Umidita suolo out of range: " + lettura.getDouble("soilMoisture")+".";
							break;
						case 6:
							notifica = "Serra " + serra.getID() + ":	[Esito] Umidita out of range: " + lettura.getDouble("humidity") + "" + "	Umidita suolo out of range: " + lettura.getDouble("soilMoisture")+".";
							break;
						case 7:
							notifica = "Serra " + serra.getID() + ":	[Esito] Temperatura out of range: " + lettura.getDouble("temperature") + "" + "	Umidita out of range: " + lettura.getDouble("humidity") + "" + "	Umidita suolo out of range: " + lettura.getDouble("soilMoisture") +".";
							break;
						default:
							break;
					}
					
					proxy.invioSegnalazioneRange(admininfo, employersinfo, notifica);
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
			
			}
		}

		return esito;
	}

	public synchronized void gestioneStatusAttuatori(int idSerra, JSONObject status) {
		
		GestioneMonitoraggioBL gestoreBL = new GestioneMonitoraggioBL();
		
		Coltivazione coltivazione = gestoreBL.getColtivazione(idSerra);
	 	
		try {
			coltivazione = ColtivazioneDAO.getColtivazioneByORMID(idSerra);
		} catch (PersistentException e1) {
			e1.printStackTrace();
		}
		
		if ( coltivazione != null ) {
		
			Serra serra = coltivazione.getSerra();
			
			String admininfo = coltivazione.getNotifyInformationAdmin();
			ArrayList<String> employersinfo = serra.getNotifyInformationEmployers();
			
			ProxyNotificationSystem proxy = ProxyNotificationSystem.getInstance();
			
			try {
				proxy.invioSegnalazioneAttuatore(admininfo, employersinfo, "Serra: " + serra.getID() + " " + status.getString("status"));
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		}
	}
	
}
