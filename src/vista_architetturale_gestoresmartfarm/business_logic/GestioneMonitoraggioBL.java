package vista_architetturale_gestoresmartfarm.business_logic;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.orm.PersistentException;

import vista_architetturale_gestoresmartfarm.entity2.Coltivazione;
import vista_architetturale_gestoresmartfarm.entity2.Sensore;
import vista_architetturale_gestoresmartfarm.entity2.Serra;
import vista_architetturale_gestoresmartfarm.entity2.SmartFarm;

public class GestioneMonitoraggioBL {

	public GestioneMonitoraggioBL() {
		
	}

	public void creaLettura(JSONObject lettura, Serra serra) {
		
		Sensore temperatura = new Sensore();
		Sensore humidity = new Sensore();
		Sensore soilMoisture = new Sensore();
		
		temperatura.setTipo("Temperatura");
		
		float temp;
		float umidita;
		float umiditaSuolo;
		
		try {
			
			temp = Float.parseFloat((String)lettura.get("temperature"));
			umidita = Float.parseFloat((String)lettura.get("humidity"));
			umiditaSuolo = Float.parseFloat((String)lettura.get("soilMoisture"));
			
			temperatura.setValore(temp);
			//temperatura.elabora();
			
			humidity.setValore(umidita);
			//humidity.elabora();
			
			soilMoisture.setValore(umiditaSuolo);
			//soilMoisture.elabora();
		
			try {
				serra.salvaLettura(lettura);
			} catch (PersistentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		} 
	
	
	}
	
	private boolean between(double x, double minVal, double maxVal) {
		return (x >= minVal && x <= maxVal);
	}

	public int checkRange(JSONObject lettura, Coltivazione coltivazione) {
		
		int esito = 0;
		
		float tempMin = coltivazione.getTempMin();
		float tempMax = coltivazione.getTempMax();
		float humMin = coltivazione.getHumMin();
		float humMax = coltivazione.getHumMax();
		float soilMin = coltivazione.getSoilMin();
		float soilMax = coltivazione.getSoilMax();
		
		double tempLett = 0;
		double humLett = 0;
		double soilLett = 0;
		
		try {
			
			tempLett = lettura.getDouble("temperature");
			humLett = lettura.getDouble("humidity");
			soilLett = lettura.getDouble("soilMoisture");
			
			
			if( between(tempLett,tempMin,tempMax) && between(humLett,humMin,humMax) && between(soilLett,soilMin,soilMax)) {
				esito = 0;
			}else {
				
				if ( !between(tempLett,tempMin,tempMax) && between(humLett,humMin,humMax) && between(soilLett,soilMin,soilMax) ) {
					esito = 1; // temperatura fuori range
				}
			
				if ( between(tempLett,tempMin,tempMax) && !between(humLett,humMin,humMax) && between(soilLett,soilMin,soilMax) ) {
					esito = 2; // umidità fuori range
				}
				
				if ( between(tempLett,tempMin,tempMax) && between(humLett,humMin,humMax) && !between(soilLett,soilMin,soilMax) ) {
					esito = 3; // soilMoisture fuori range
				}
				
				if ( !between(tempLett,tempMin,tempMax) && !between(humLett,humMin,humMax) && between(soilLett,soilMin,soilMax) ) {
					esito = 4; // temperatura e umidità fuori range
				}
				
				if ( !between(tempLett,tempMin,tempMax) && between(humLett,humMin,humMax) && !between(soilLett,soilMin,soilMax) ) {
					esito = 5; // temperatura e soilMoisture fuori range 
				}
				
				if ( between(tempLett,tempMin,tempMax) && !between(humLett,humMin,humMax) && !between(soilLett,soilMin,soilMax) ) {
					esito = 6; // umidità e soilMoisture fuori range 
				}
				
				if ( !between(tempLett,tempMin,tempMax) && !between(humLett,humMin,humMax) && !between(soilLett,soilMin,soilMax) ) {
					esito = 7; // tutto fuori range
				}
			}
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		return esito;
		
	}
	
	public Coltivazione getColtivazione(int idSerra) {
		
		SmartFarm smartfarm = null;
		
		try {
			smartfarm = SmartFarm.caricaSmartFarm();
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("	Prelevata coltivazione: "+ smartfarm.getColtivazioniAttive(idSerra).getID());
		
		return smartfarm.getColtivazioniAttive(idSerra);
		
	}
	
}
