package vista_architetturale_serra.serra_side_proxy;

import java.util.ArrayList;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.*;

import vista_architetturale_serra.serra_side_controller.CoordinatorAttuatoreSerra;

public class SerraWorker extends Thread {

	private MqttMessage message;
	private String topicInvioStatus;
	private String broker;
	private String clientId;
	private int idSerra;
	

	public SerraWorker(MqttMessage message, int idSerra, String broker, String clientId, String topicInvioStatus) {
		super();
		this.message = message;
		this.topicInvioStatus = topicInvioStatus;
		this.idSerra = idSerra;
		this.broker = broker;
		this.clientId = clientId;
	}

	@Override
	public void run(){

		try {
			
			String requestType = message.toString();
			
			switch(requestType){

				case "AttivaCondizionamento":
					break;
			
				case "AttivaIrrigazione":
				
					CoordinatorAttuatoreSerra coordAtt = CoordinatorAttuatoreSerra.getInstance();
					int esito = coordAtt.invioIrrigazione();
					
			        JSONObject jsonReply = new JSONObject();
			        
			        if(esito == 0) {
			        	jsonReply.put("status", "Attuatore avviato correttamente.");
			        }else if(esito == 1) {
			        	jsonReply.put("status", "Attuatore non avviato correttamente.");
			        }
			        
			        jsonReply.put("idSerra", idSerra);
			        
			        MemoryPersistence persistence = new MemoryPersistence();

			        try {
			        	
			            MqttClient sampleClient = new MqttClient(this.broker, this.clientId+"_Status", persistence);
			            MqttConnectOptions connOpts = new MqttConnectOptions();
			            connOpts.setCleanSession(true);
			         
			            sampleClient.connect(connOpts);
			            System.out.println("[Serra"+idSerra+"] Connesso al broker: "+broker);
			            
			            MqttMessage message = new MqttMessage();
						message.setPayload(jsonReply.toString().getBytes());
			            sampleClient.publish(this.topicInvioStatus, message);
			            System.out.println("	["+this.topicInvioStatus+"] Status inviato.");
			            
			            sampleClient.disconnect();
			            System.out.println("[Serra"+idSerra+"] Disconnesso");
			            
			        } catch(MqttException me) {
			            System.out.println("reason "+me.getReasonCode());
			            System.out.println("msg "+me.getMessage());
			            System.out.println("loc "+me.getLocalizedMessage());
			            System.out.println("cause "+me.getCause());
			            System.out.println("excep "+me);
			            me.printStackTrace();
			        }
				
					break;
				
				default:
					System.out.println("No match type");		
			}


		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
