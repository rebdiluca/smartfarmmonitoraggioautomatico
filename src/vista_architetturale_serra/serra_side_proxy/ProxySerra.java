package vista_architetturale_serra.serra_side_proxy;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONObject;


public class ProxySerra implements MqttCallback{
	
	 private String broker;
	 private String clientId;
	 private String topicInvioStatus;
	 private String topicInvioLetture;
	 private int idSerra;
	    
	 private volatile static ProxySerra proxy = null;

	 private ProxySerra() {	
	 }
		
	 public static ProxySerra getInstance() {
		 if (proxy == null) {
			 synchronized(ProxySerra.class){ 
				 if(proxy == null){
					 proxy = new ProxySerra();
				 }
			 }
		 }
		 return proxy;
	 }
 
	public String getTopicInvioLetture() {
		return topicInvioLetture;
	}

	public void setTopicInvioLetture(String topicInvioLetture) {
		this.topicInvioLetture = topicInvioLetture;
	}

	public int getIdSerra() {
		return idSerra;
	}

	public void setIdSerra(int idSerra) {
		this.idSerra = idSerra;
	}

	public String getBroker() {
		return broker;
	}

	public void setBroker(String broker) {
		this.broker = broker;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getTopicInvioStatus() {
		return topicInvioStatus;
	}

	public void setTopicInvioStatus(String topicInvioStatus) {
		this.topicInvioStatus = topicInvioStatus;
	}

	public void subscribe(String topicName){
		
		try {
			MqttClient client = new MqttClient(this.broker, this.clientId);
			client.connect();
			client.setCallback(this);
			client.subscribe(topicName);
			System.out.println("[Sottoscrizione]:" + topicName + " topic dei comandi.");
		} catch (MqttException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void connectionLost(Throwable arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void messageArrived(String topicRicezione, MqttMessage message) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("["+topicRicezione+"] Comando ricevuto.\n\n");
		SerraWorker worker = new SerraWorker(message, this.idSerra, this.broker, this.clientId, this.topicInvioStatus);
		worker.start();
	}
	
	public void invioValori(JSONObject lettura){
		
        MemoryPersistence persistence = new MemoryPersistence();

        try {
        	
            MqttClient sampleClient = new MqttClient(broker, clientId+"_Letture", persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            sampleClient.connect(connOpts);
            System.out.println("[Serra "+this.idSerra+"] Connesso al broker: "+broker);
            
            MqttMessage message = new MqttMessage();
			message.setPayload(lettura.toString().getBytes());
            sampleClient.publish(this.topicInvioLetture, message);
            System.out.println("	["+this.topicInvioLetture+"] Lettura pubblicata.");
            
            sampleClient.disconnect();
            System.out.println("[Serra "+idSerra+"] Disconnesso.\n\n");
            
        } catch(MqttException me) {
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
            me.printStackTrace();
        }
		
	}
}
