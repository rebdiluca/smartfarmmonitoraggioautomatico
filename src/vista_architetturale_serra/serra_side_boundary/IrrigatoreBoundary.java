package vista_architetturale_serra.serra_side_boundary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class IrrigatoreBoundary implements IAttuatore {

	@Override
	public int doAction() throws IOException {
		
		String line = new String();
		int esito = 0;
		
		String testCommand = "python TestRedled.py";
		Process test = Runtime.getRuntime().exec(testCommand);
		
		try {
			test.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    
		BufferedReader bri = new BufferedReader(new InputStreamReader(test.getInputStream()));
	     
	    if((line = bri.readLine()) != null){
	    	 if(line.compareTo("Errore") == 0)
	    		 esito = 1;	 
	    }
	    
	    bri.close();
	    
	    String irrCommand = "python Yellowled.py";
		//Process activity = Runtime.getRuntime().exec(irrCommand + " on");
	    Runtime.getRuntime().exec(irrCommand + " on");
	    
		try {
			test.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return esito;
	}
	
}
