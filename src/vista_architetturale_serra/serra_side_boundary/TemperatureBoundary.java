package vista_architetturale_serra.serra_side_boundary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TemperatureBoundary implements ISensore {

	private String tipo;
	
	public TemperatureBoundary(String tipo) {
		super();
		this.tipo = tipo;
	}
	
	@Override
	public Object getValue() throws IOException {
		
		
		String line = new String();
		String x = new String();
		
		String command = "python Sensore.py";
		Process p = Runtime.getRuntime().exec(command + " temperature");
		
		try {
			p.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
	     
	    if((line = bri.readLine()) != null){
	    	 x = line;
	    }
	    
	    bri.close();
		
		return x;
	}

	@Override
	public String getTipo() {
		return tipo;
	}

}

