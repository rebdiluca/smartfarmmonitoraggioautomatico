package vista_architetturale_serra.serra_side_boundary;

import java.io.IOException;

public interface IAttuatore {
	int doAction() throws IOException;
}
