package vista_architetturale_serra.serra_side_controller;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import vista_architetturale_serra.serra_side_boundary.ISensore;
import vista_architetturale_serra.serra_side_proxy.ProxySerra;

public class CoordinatorMonitoraggioSerra {
	
	private volatile static CoordinatorMonitoraggioSerra c = null;
	
	private CoordinatorMonitoraggioSerra(){
	}

	public static CoordinatorMonitoraggioSerra getInstance(){
		if (c == null) {
			synchronized(CoordinatorMonitoraggioSerra.class){
				if(c == null){
					c = new CoordinatorMonitoraggioSerra();
				}
			}
		}
		return c;
	}
	
	public void invioValori(int idSerra, ArrayList<ISensore> sensori, ProxySerra proxySerra){
		
		JSONObject lettura = new JSONObject();
		
		try {
			
			/*** INSERIMENTO ID SERRA PER IL RICONOSCIMENTO DELLA LETTURA LATO SMARTFARMIOTSERVER ***/
			lettura.put("idSerra", idSerra);
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		for(int i=0; i<sensori.size(); i++){
			
			try {
				
				/*** PRELIEVO DATI DAL SENSORE i ***/
				String tipo = sensori.get(i).getTipo();	
				String  valore = (String) sensori.get(i).getValue();
				
				/*** INSERIMENTO DATI DEL SENSORE i NEL MESSAGGIO DA INVIARE ***/
				lettura.put(tipo, valore);
				
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		}
		
		/*** LETTURA AL PROXY PER INVIARLA SUL BROKER ***/
		proxySerra.invioValori(lettura);
		
	}
}
