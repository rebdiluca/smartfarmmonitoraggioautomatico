package vista_architetturale_serra.serra_side_controller;


import java.io.IOException;

import vista_architetturale_serra.serra_side_boundary.IAttuatore;
import vista_architetturale_serra.serra_side_boundary.IrrigatoreBoundary;

public class CoordinatorAttuatoreSerra {

	private volatile static CoordinatorAttuatoreSerra c=null;

	private CoordinatorAttuatoreSerra (){

	}
	public static CoordinatorAttuatoreSerra getInstance(){
		if (c == null) {
			synchronized(CoordinatorAttuatoreSerra.class){
				if(c == null){
					c = new CoordinatorAttuatoreSerra();
				}
			}
		}
		return c;
	}
	
	public int invioIrrigazione(){
		IAttuatore irrigatore = new IrrigatoreBoundary();
		int esito = 0;
		
		try {
			esito = irrigatore.doAction();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return esito;
	}
	
}
