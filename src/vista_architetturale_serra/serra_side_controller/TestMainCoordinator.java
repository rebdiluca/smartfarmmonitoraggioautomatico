package vista_architetturale_serra.serra_side_controller;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import vista_architetturale_serra.serra_side_proxy.ProxySerra;
import vista_architetturale_serra.serra_side_boundary.*;

public class TestMainCoordinator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		// creazione coordinator
		CoordinatorMonitoraggioSerra coordMon = CoordinatorMonitoraggioSerra.getInstance();
		
		
		// creazione sensori
		TemperatureBoundary tempSensor = new TemperatureBoundary("temperature");
		HumidityBoundary humSensor = new HumidityBoundary("humidity");
		SoilMoistureBoundary soilSensor =  new SoilMoistureBoundary("soilMoisture");
		ArrayList<ISensore> sensori = new ArrayList<ISensore>();
		sensori.add(tempSensor);
		sensori.add(humSensor);
		sensori.add(soilSensor);
		
		// prelievo numero serra
		int idSerra = Integer.parseInt(args[0]);
		System.out.println("Serra " + idSerra + " avviata.");
		
		
		
		/**** IMPOSTAZIONE PROXY SERRA ****/
		
		
		ProxySerra proxySerra = ProxySerra.getInstance();
		proxySerra.setIdSerra(idSerra);
		
		//fornire il broker per l'invio delle letture e la ricezione dei comandi da SmartFarmIotServer
		proxySerra.setBroker("tcp://broker.hivemq.com:1883");
		proxySerra.setClientId("Serra"+idSerra);
		
		//fornire il topic su cui inviare lo status
		proxySerra.setTopicInvioStatus("serre/statusAttuatori");
		
		//fornire il topic su cui inviare le letture
		proxySerra.setTopicInvioLetture("serre/letture");
		
		//topic per ricevere i comandi
		proxySerra.subscribe("serre/comandi/serra"+idSerra);
		
		
		/**** FINE IMPOSTAZIONE PROXY SERRA ****/
		
		
		
		/**** AVVIO TIMER PER L'INVIO DELLE LETTURE ****/
		
		int sec = 30;
		
		while ( true ) {
			
			coordMon.invioValori(idSerra, sensori, proxySerra);
			
			try {
				Thread.sleep(sec*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	
	}

}

