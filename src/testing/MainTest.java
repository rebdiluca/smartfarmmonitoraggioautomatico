package testing;


import org.orm.PersistentException;
import vista_architetturale_gestoresmartfarm.boundary.ProxySerra;

public class MainTest {

	public static void main(String[] args) throws PersistentException {
		
		ProxySerra proxy = ProxySerra.getInstance();
		
		//setting broker per la ricezione (status e letture) e per l'invio dei comandi
		proxy.setBroker("tcp://broker.hivemq.com:1883"); 
		proxy.setClientId("SmartFarmIotServer"); 
		
		//setting topic per l'invio dei comandi in caso di out-of-range
		proxy.setTopicInvioComandi("serre/comandi");
		
		//sottoscrizione al topic serre/+ per ricevere sia letture che status attuatori (vedi commenti sotto)
		proxy.subscribe("serre/+");

		/*
				Topic: serre/letture
				Topic: serre/status
		*/
		
	}

}
